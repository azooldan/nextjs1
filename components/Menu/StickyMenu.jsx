import React, { Component } from 'react';
import HamburgerMenu from './HamburgerMenu'

class StickyMenu extends React.Component {

	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);
		
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}

  handleScroll = (event) => {
		const logo = document.getElementById("logo");
		const links = document.getElementById("links");
		const topMenu = document.getElementById("topMenuContainer");

		if(window.scrollY > 10) {
  			logo.classList.add("smallLogo");
				links.classList.add("smallLinks");
				topMenu.classList.add("topMenuShadow");
			
		}
		if(window.scrollY === 0){
  			logo.classList.remove("smallLogo");
				links.classList.remove("smallLinks");
				topMenu.classList.remove("topMenuShadow");
		}
  }

  render() {
    return(
			<div className="topMenuContainer" id="topMenuContainer" data-aos='fade-in' data-aos-duration='800'>

				<img src="img/amanda_logo.png" alt="AMANDA Technology" className="logo" id="logo"/>

				<HamburgerMenu></HamburgerMenu>

				<div className="links" id="links">
					<a href='#'>
						<h4 className="homeText">Home</h4>
					</a>
					<a href='#Benefits'>
						<h4>Benefits</h4>
					</a>
					<a href='#Features'>
						<h4>Features</h4>
					</a>
					<a href='#Services'>
						<h4>Services</h4>
					</a>
					<a href='#'>
						<h4>Technology</h4>
					</a>
					<a href='#'>
						<h4>Team</h4>
					</a>
					<a href='#'>
						<h4>FAQ</h4>
					</a>
					<a href='#'>
						<h4>Kontakt</h4>
					</a>
				</div>

				<style jsx>{`
					.hamburger-menu {
						display: none;
					}
					.topMenuContainer{
						background-color: #272d3a;
						display: flex;
						flex-direction: row;
						position: fixed;
						width: 100%;
						top: 0;
						z-index: 100;
					}
					.topMenuShadow {
						box-shadow: 0 4px 4px rgba(0, 0, 0, 0.1);
					}
					.links{
						display: flex;
						flex-direction: row;
						justify-content: flex-end;
						padding-top: 40px;
						width: 100%;
						padding-right: 50px;
						transition: all 0.4s ease-out;
					}
					.smallLinks{
						padding-top: 10px;
						transition: all 0.4s ease-out;
					}
					a {
						padding-right: 40px;
						font-family: 'Raleway', serif;
						font-size: 22px;
						text-decoration: none !important;
					}
					h4:hover { 
						color: #ff8f00;; 
					}
					.logo {
						margin-left: 120px ;
						margin-top: 60px ;
						height: auto;
						width: 30%;
						transition: all 0.4s ease-out;
					}
					.smallLogo {
						margin-left: 120px ;
						margin-top: 5px ;
						margin-bottom: 5px ;
						height: auto;
						width: 20%;
						transition: all 0.4s ease-out;
					}
					h4 {
						color: #f7f7f7;
							-webkit-transition: color 0.2s;
							-moz-transition:    color 0.2ss;
							-ms-transition:     color 0.2ss;
							-o-transition:      color 0.2ss;
							transition:         color 0.2ss;
					}
					.homeText {
						color: #ee0f6f;
					}
					
					@media (max-width: 767px) {
						
						.logo {
							margin-left: 10px ;
							margin-top: 10px ;
							height: auto;
							width: 30%;
						}
						.links{
							display: none;
						}
						.topMenuContainer {
							width: 100%;
						}
						.smallLogo {
							margin-left: 5px ;
							margin-top: 5px ;
							height: auto;
							width: 20%;
							transition: all 0.4s ease-out;
						}
					}
					@media (min-width: 768px) and (max-width: 1367px) {
						h4 {
							font-size: 16px;
						}
					}	
					@media (min-width: 1367px) {
						.logo {
							margin-left: 200px ;
							margin-top: 10px ;
							height: auto;
							width: 25%;
						}
						.smallLogo {
							margin-left: 200px ;
							margin-top: 10px ;
							height: auto;
							width: 15%;
							transition: all 0.2s ease-out;
						}
						h4 {
							font-size: 18px;
						}
					}



				`}</style>
			</div>
		)
	}
}
 
export default StickyMenu;



const HamburgerMenu = () => (
  <div className="hamburger-menu">
    <input id="menu__toggle" type="checkbox" />
    <label className="menu__btn" htmlFor="menu__toggle">
      <span></span>
    </label>

    <ul className="menu__box">
      <li><a className="menu__item" href="#Benefits">Benefits</a></li>
      <li><a className="menu__item" href="#Features">Features</a></li>
      <li><a className="menu__item" href="#Services">Services</a></li>
      <li><a className="menu__item" href="#Technology">Technology</a></li>
      <li><a className="menu__item" href="#Team">Team</a></li>
      <li><a className="menu__item" href="#FAQ">FAQ</a></li>
      <li><a className="menu__item" href="#Kontakt">Kontakt</a></li>
    </ul>

    <style jsx>{`
      .hamburger-menu {
        display: none;
      }

      @media (max-width: 1150px) {
        .hamburger-menu {
          display: flex;
        }

        #menu__toggle {
          opacity: 0;
        }

        #menu__toggle:checked ~ .menu__btn > span {
          transform: rotate(45deg);
        }
        #menu__toggle:checked ~ .menu__btn > span::before {
          top: 0;
          transform: rotate(0);
        }
        #menu__toggle:checked ~ .menu__btn > span::after {
          top: 0;
          transform: rotate(90deg);
        }

        #menu__toggle:checked ~ .menu__box {
          visibility: visible;
          right: 0;
        }

        .menu__item:hover {
          background-color: #CFD8DC;
        }

        .menu__btn {
          display: flex;
          align-items: center;
          position: fixed;
          top: 8px;
          right: 20px;
          width: 26px;
          height: 26px;
        }

        .menu__btn > span,
        .menu__btn > span::before,
        .menu__btn > span::after {
          display: block;
          position: absolute;

          width: 100%;
          height: 2px;

          background-color: #616161;

          transition-duration: .25s;
        }
        
        .menu__btn > span::before {
          content: '';
          top: -8px;
        }
        .menu__btn > span::after {
          content: '';
          top: 8px;
        }

        .menu__box {
          position: fixed;
          visibility: hidden;
          top: 7%;
          right: -100%;
          border-radius: 10px;
          width: 150px;
          height: auto;


          list-style: none;

          background-color: #ECEFF1;
          box-shadow: 1px 0px 6px rgba(0, 0, 0, .2);

          transition-duration: .25s;
        }

        .menu__item {
          display: block;
          padding: 5px;
          color: #333;

          font-family: 'Roboto', sans-serif;
          font-size: 14px;
          font-weight: 200;

          text-decoration: none;

          transition-duration: .25s;
        }
      }
    `}</style>
</div>
)

export default HamburgerMenu;
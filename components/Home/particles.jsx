import Particles from 'react-particles-js';
import InfoBlock from "./infoBlock";

class ParticlesBackground extends React.Component {	
	constructor(props) {
		super(props);
		this.state = {

		}
	}

	componentDidMount() {
		const canvas = document.getElementsByTagName('canvas')[0];



		const infoBlockHeight = document.getElementsByClassName('infoBlock')[0].clientHeight;
		const image = document.getElementsByClassName('backgroundImage')[0];
		const bs = document.getElementsByClassName('backgroundImage')[0].clientHeight;
		console.log('before', infoBlockHeight, bs)
		let height = infoBlockHeight;

		image.style.height = `${infoBlockHeight}px`;
		canvas.height = height;
		canvas.width = window.innerWidth;

		const a = document.getElementsByClassName('infoBlock')[0].clientHeight;
		const b = document.getElementsByClassName('backgroundImage')[0].clientHeight;
		console.log('after', a, b)
	}

  render() {
    return(
		<div>

			<div className="backgroundImage"> 
				<img src="img/particles_background.jpg" width="100%" height="100%"/>
			</div>

			<div className="particlesContainer"> 
					<Particles
						params={{
							"particles": {
								"line_linked": {"color":"#FFFFFF"},
								"number": {"value": 75},
								"size": {"value": 3}
							},
							"interactivity": {
								"events": {
									"onhover": {
										"enable": true,
										"mode": "repulse"
									}
								}
							},
						}}
						style={{
							background: `rgb(39,45,58, 0.6)`,
							position : 'relative', 
							zIndex: '-1',
							height: '100%'
						}}
						height={'100%'}
						width={'100%'}
					/>
			</div>
			
			<div className={'infoBlock'}>
				<InfoBlock></InfoBlock>
			</div>

			<style jsx>{`
				.infoBlock {
						padding-top: 200px 
				}
				.particlesContainer {
					position: absolute;
				}
				.backgroundImage {
					position:absolute;
					width: 100%;
					top: 0;
					z-index: -2;
				}
				}
			`}</style>
		</div>
	)
}
}

export default ParticlesBackground
				// @media (max-width: 767px) {
				// 	.backgroundImage {
				// 		height: 70%;
				// 	}
				// }
				// @media (min-width: 768px) and (max-width: 1367px) {
				// 	.backgroundImage {
				// 		height: 119%;
				// 	}
				// }	

				// @media (min-width: 1368px) {
				// 	.backgroundImage {
				// 		height: 129%;
				// 	}
				// }
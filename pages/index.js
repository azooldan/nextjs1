import React from 'react';
import Head from 'next/head'

import StickyMenu from "../components/Menu/StickyMenu";
import Home from "../components/Home/index";
import Benefits from "../components/Benefits/index";
import Features from "../components/Features/index";
import Services from "../components/Services/index";

import AOS from 'aos';


class Index extends React.Component {
  componentDidMount(){
    AOS.init({
      duration : 2000
    })
  }
  render() {
    return(
      <div className="container">
        <Head>
          <title>AMANDA Technology</title>
          <link rel="icon" href="/favicon.ico" />
          <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link href='https://fonts.googleapis.com/css?family=Raleway:500,700,800' rel='stylesheet' type='text/css'/>        
        </Head>
        <main>
            <StickyMenu></StickyMenu>
            <Home></Home>
            <Benefits></Benefits>
            <Features></Features>
            <Services></Services>
        </main>
      </div>
    )
  }
}

export default Index
